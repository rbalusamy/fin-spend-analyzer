package com.hcl.financial.analyzer.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.financial.analyzer.model.CustomerTransactionDetails;

@Repository
public interface TransactionRepository extends CrudRepository<CustomerTransactionDetails, Integer>{
	
	
	List<CustomerTransactionDetails> findByCustomerId(Long id);
	

}
