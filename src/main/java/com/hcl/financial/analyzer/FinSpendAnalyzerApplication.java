package com.hcl.financial.analyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class FinSpendAnalyzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinSpendAnalyzerApplication.class, args);
	}
}
