package com.hcl.financial.analyzer.controller;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.financial.analyzer.model.CustomerTransactionDetails;
import com.hcl.financial.analyzer.repository.TransactionRepository;

@RestController
@RequestMapping("/bank")
@CrossOrigin("*")

public class TransactionController {
	@Autowired
	TransactionRepository repository;

	@PostMapping("/transaction")
	
	public void doTransaction(@Validated @RequestBody CustomerTransactionDetails transaction){
		repository.save(transaction);
	}
	
	
	@RequestMapping(value="/display/{customerId}",    
    produces={ MediaType.APPLICATION_JSON_VALUE},method=RequestMethod.GET)
	public CustomerTransactionDetails getCustomerDetail(@PathVariable("customerId") int  customerId){
		
		CustomerTransactionDetails details = repository.findOne(customerId);
				
		return repository.findOne(customerId);
	}
	
	@GetMapping("/transdisplay/{customerId}")
	public List<CustomerTransactionDetails> getCustomerDetails(@PathVariable("customerId") int  customerId){
		
					
		return repository.findByCustomerId(Long.valueOf(customerId));
		
	}
	
	@GetMapping("/transaction/report/{customerId}")
	public List<CustomerTransactionDetails> getCustomerTransactions(@PathVariable("customerId") int  customerId){
		
					
		return repository.findByCustomerId(Long.valueOf(customerId));
		
	}
	
	public CustomerTransactionDetails getMock(){
		CustomerTransactionDetails transaction = new CustomerTransactionDetails();
		transaction.setAmount(1000);
		transaction.setDate(new Date());
		transaction.setTransactionId(10);
		
		
		return transaction;
	}
}
