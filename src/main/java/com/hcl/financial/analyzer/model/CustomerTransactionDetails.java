package com.hcl.financial.analyzer.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "transactions")
@EntityListeners(AuditingEntityListener.class)
public class CustomerTransactionDetails {
	public CustomerTransactionDetails(){
		
	}

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 int transactionId;
	 
	 Long customerId =0l;
	 String description;
	 double amount;
	
	 /*@Temporal(TemporalType.DATE)
	 @DateTimeFormat(style = "yyyy-MM-dd")
	 @NotNull*/
	 @CreatedDate
	 Date date;
	 //@Enumerated(EnumType.STRING)
	 public String paymentType;

	
		public int getTransactionId() {
		return transactionId;
	}


	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}


	public Long getCustomerId() {
		return customerId;
	}


	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getPaymentType() {
		return paymentType;
	}


	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}


		public enum type {
			Credit, Debit
		}

}
